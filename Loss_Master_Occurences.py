#############################################################################################
# Author:   Kyle Hine (E017963)                                                             #
# Purpose:  Count the total unique occurences within the                                    #
#           "FIELD LABEL" column on the "LOSS MASTER DIFFERENCES REPORT"                    #
#           input file location:                                                            #
#           \\aoins.com\RootNoProblemClaims\NoProblemClaims\Reports\PROD_Lossmaster_Compare #
# Input:    sample input available at the bottom of the page                                #
#                                                                                           #
#############################################################################################
import os
import glob

def wordCounter(dictionary, row, position):
    """Stores the number of occurences in dictionary """
    word = row[position][1:]        #remove the first char (is a space)
    if word not in dictionary:
        dictionary[word] = 1
    else:
        dictionary[word] += 1

### open the most recent file in the directory
file_path = (r"\\aoins.com\RootNoProblemClaims\NoProblemClaims\Reports\PROD_Lossmaster_Compare")
sorted_list = sorted(os.listdir(file_path))
newest_filename = sorted_list[-1]   #minimum is the most recent [-1] second most is [-2]
full_name = file_path + '\\' + newest_filename
print full_name + '  '              #adding two spaces for gitlab newline markdown
fh = open(full_name,"r+")

### collect the each line and store in document{}
document ={}
count = 0
for line in fh.read().split("\n"):  #grab each line ending with newline
    if line.startswith("1")\
    or line.startswith("0")\
    or line.startswith(" R")\
    or line.startswith(" -"):
        continue
    else:                           #ignores header information
            if len(line) > 0:       #avoids EOF
                document[count] = line
                count += 1

fh.close()                          #close the file

### collect the FIELD LABEL's
wordCount={}
for val in document.values():

    row = filter(None, val.split('  ')) #filter removes spaces
#    print len(row), row            #good debug point to view
    
    if len(row) > 4:                #check for rows of different lengths
        wordCounter(wordCount, row, 4)
    else:
        wordCounter(wordCount, row, 0)

### print wordCount{} totals
for k,v in wordCount.items():
        print k, v, '  '            #adding two spaces for gitlab newline markdown

# TODO: add colors?
# print green: '\033[92m'
# print blue:  '\033[94m'

#Sample .txt file input:
"""
1PROCESS DATE: 08/04/2015                           LOSS MASTER DIFFERENCES REPORT                    JC20CC29            PAGE      1
 RUN DATE:     08/03/2015                                                                             SN20CC29                       
 RUN TIME:     08:28 PM                                                                               RPTCC2904-1                    
0CLM REF #  COV REF #       CLAIM        COV     FIELD LABEL           BEFORE FIX            AFTER FIX                               
 ------------------------------------------------------------------------------------------------------------------------------------
 700047445  700063154  300-0042150-2014  096     SUBR CO IND           0                     1                                       
 ------------------------------------------------------------------------------------------------------------------------------------
 700061061  700081946  300-0055765-2014  042     CLOSE DTE GRP         20150320              20150803                                
 ------------------------------------------------------------------------------------------------------------------------------------
 700168074  700230687  300-0046011-2015  042     CLOSE DTE GRP         20150804              20150724                                
                                                 REOPEN DTE GRP        20150804              00000000                                
                                                 ORIG EST                           0.00              4,100.00                       
                                                 EST CHNG                       7,700.00              3,600.00                       
                                                 REOPEN CODE           1                     0                                       
 ------------------------------------------------------------------------------------------------------------------------------------
 700169885  700239589  300-0047822-2015  080     SALV DTE GRP          20150803              20150731                                
 ------------------------------------------------------------------------------------------------------------------------------------
"""

